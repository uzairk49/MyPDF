package com.intern.mypdf

import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.pspdfkit.configuration.activity.PdfActivityConfiguration

class PdfActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val uri = Uri.parse("file:///android_asset/sample.pdf")
        val config = PdfActivityConfiguration.Builder(this).build()
        com.pspdfkit.ui.PdfActivity.showDocument(this, uri, null, config)
    }
}